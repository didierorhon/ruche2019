#+TITLE: Étude du code de la station météo ESP32 d'Elektor
#+AUTHOR: Thierry
#+LANGUAGE: FR
#+LATEX_HEADER: \usepackage[latin1]{inputenc}
#+LATEX_HEADER: \usepackage[frenchb]{babel}
#+LATEX_HEADER: \usepackage[a4paper,left=10mm,right=10mm,top=5mm,bottom=5mm]{geometry}

* Structure du code
** Hiérarchie globale
  #+BEGIN_SRC sh :results verbatim :exports yes
  tree esp32weatherstation -I build --prune -d
  #+END_SRC 

  #+RESULTS:
  : esp32weatherstation
  : ├── data
  : └── sds011 library
  :     └── SDS011
  :         └── examples
  :             └── sds011_test
  : 
  : 5 directories

** Fichiers ino
  #+BEGIN_SRC sh :results verbatim :exports yes
  tree esp32weatherstation -I build -P *.ino  --prune 
  #+END_SRC 

  #+RESULTS:
  #+begin_example
  esp32weatherstation
  ├── esp32weatherstation.ino
  ├── network.ino
  ├── sds011 library
  │   └── SDS011
  │       └── examples
  │           └── sds011_test
  │               └── sds011_test.ino
  ├── serialHandler.ino
  ├── storePrefs.ino
  └── uploadData.ino

  4 directories, 6 files
  #+end_example
** Fichiers H
  #+BEGIN_SRC sh :results verbatim :exports yes
  tree esp32weatherstation -I build -P *.h  --prune 
  #+END_SRC 

  #+RESULTS:
  #+begin_example
  esp32weatherstation
  ├── datastore.h
  ├── Honnywhell.h
  ├── RainSensor.h
  ├── sds011 library
  │   └── SDS011
  │       └── SDS011.h
  ├── sslCertificate.h
  ├── webfunctions.h
  └── WindSensor.h

  2 directories, 7 files
  #+end_example

** Fichiers CPP
  #+BEGIN_SRC sh :results verbatim :exports yes
  tree esp32weatherstation -I build -P *.cpp -L 1 --prune 
  #+END_SRC 

  #+RESULTS:
  : esp32weatherstation
  : ├── datastore.cpp
  : ├── Honnywhell.cpp
  : ├── RainSensor.cpp
  : ├── webfunctions.cpp
  : └── WindSensor.cpp
  : 
  : 0 directories, 5 files

** Fichiers HTML
  #+BEGIN_SRC sh :results verbatim :exports yes
  tree esp32weatherstation -I build -P *.html --prune 
  #+END_SRC 

  #+RESULTS:
  : esp32weatherstation
  : └── data
  :     └── index.html
  : 
  : 1 directory, 1 file

* Les fichiers INO
  Notons que les fichiers INO sont concaténés dans l'ordre
  alphabétique avant compilation.

  #+BEGIN_SRC sh :results verbatim :exports yes
  tree esp32weatherstation -I build -P *.ino -L 1 --prune
  #+END_SRC 

  #+RESULTS:
  : esp32weatherstation
  : ├── esp32weatherstation.ino
  : ├── network.ino
  : ├── serialHandler.ino
  : ├── storePrefs.ino
  : └── uploadData.ino
  : 
  : 0 directories, 5 files
  
** esp32weatherstation.ino
*** Commentaire introductif
    Notons la description du fichier:
    - nom
    - matériel utilisé
    - bibliothèques utilisées (ainsi que leur version)

#+BEGIN_SRC c
 /******************************************************************************************
 * 
 * Elektor ESP32 based Weaterstation 
 * HW: EPS32-PICO-D4 on PCB 180468
 * 
 * Librarys requiered:
 * 
 * From Library Manager
 * Adafruit BME280 Library 1.0.7 by Adafruit
 * Adafruit Unified Sensor 1.0.2 by Adafruit
 * ArduinoJson 6.x.x by Benoit Blanchon
 * PubSubClient by Nick O'Leary 
 * CRC32 by Christopher Baker
 * 
 *********************************************************************************************/
#+END_SRC

*** Préambule
**** Inclusions

#+BEGIN_SRC c
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include "sslCertificate.h"
#include <WebServer.h>
#include <SPIFFS.h>
#include <Preferences.h>
#include <Adafruit_BME280.h>
#include <Wire.h>
#include <SDS011.h>
#include "Honnywhell.h"
#include "WindSensor.h"
#include "RainSensor.h"
#include "datastore.h"
#include <ArduinoJson.h>
#include <PubSubClient.h>
#+END_SRC

**** Macros
#+BEGIN_SRC c
#define solarRelay 18
#define measBatt 34

#define APPin 22
#define APLed 19
#define STALed 23
#define windDirPin 32
#define windSpeedPin 33
#define rainPin 27

#define battLow 11
#define battFull 13.5
#define battInterval 2000

#define bmeAddress 0x76
#define bmeInterval 5000 //ms = 5 seconds

#define sdsInterval 60000 //ms = 1 minute

#define lastConnectedTimeout 600000 //ms = 10 minutes: 10 * 60 * 1000

#define hourMs 3600000 //ms (60 * 60 * 1000 ms)
#+END_SRC

**** Variables globales
 #+BEGIN_SRC c
//thingspeak api and fields numbers
//https://thingspeak.com/channels/535447/private_show
bool thingspeakEnabled;
String thingspeakApi;
int tsfWindSpeed = 0;
int tsfWindDir = 0;
int tsfRainAmount = 0;
int tsfTemperature = 0;
int tsfHumidity = 0;
int tsfAirpressure = 0;
int tsfPM25 = 0;
int tsfPM10 = 0;

//senseBox IDs
bool senseBoxEnabled;
String senseBoxStationId;
String senseBoxTempId;
String senseBoxHumId;
String senseBoxPressId;
String senseBoxWindSId;
String senseBoxWindDId;
String senseBoxRainId;
String senseBoxPM25Id;
String senseBoxPM10Id;

unsigned long uploadInterval = hourMs;
bool uploaded = false;


bool prevWindPinVal = false;
bool prevRainPinVal = false;

//current sensor values
int windDir = 0; //degrees
float windSpeed = 0; //m/s
int beaufort = 0;
String beaufortDesc = "";
float windSpeedAvg = 0;
float windDirAvg = 0;
float rainAmountAvg = 0;

float temperature = 0; //*C
float humidity = 0; //%
float pressure = 0; //hPa
bool bmeRead = 0;

float PM10 = 0; //particle size: 10 um or less
float PM25 = 0; //particle size: 2.5 um or less

float batteryVoltage = 0; //v
float batteryCharging = false;

//serial variables
String serialIn;
bool serialRdy = false;
bool volatile hasBME280 = false;
unsigned long lastBMETime = 0;
unsigned long lastSDSTime = 0;
unsigned long lastUploadTime = 0;
unsigned long lastAPConnection = 0;
unsigned long lastBattMeasurement = 0;

WindSensor ws(windSpeedPin, windDirPin);
RainSensor rs(rainPin);
Adafruit_BME280 bme;
 #+END_SRC 
**** Paramètres codés à la compilation 
     Ici sont définis des paramètres définis à la compilation, par
 opposition des paramètres qui seront modifiables par l'utilisateur en
 cours d'utilisation.
 #+BEGIN_SRC c
  #define USE_SDS011
 //#define USE_HONNYWHELLHPM
 #+END_SRC 

**** Options paramètriques de compilation
 #+BEGIN_SRC c
#ifdef USE_SDS011
/* If SDS011 is connected */
SDS011 sds(Serial1);
#endif

#ifdef USE_HONNYWHELLHPM
/* If Honnywhell sensor is connected */
/* Warning Code is untested and not supported */
HonnywhellHPM hpm(Serial1);
#endif

 #+END_SRC
**** Suite des variables globales
 #+BEGIN_SRC c
//webserver, client and secure client pointers
WebServer* server = NULL;
WiFiClient* client = NULL;
WiFiClientSecure* clientS = NULL;

WiFiClient espClient;                       // WiFi ESP Client  
PubSubClient mqttclient(espClient);             // MQTT Client 

Preferences pref;

TaskHandle_t MQTTTaskHandle = NULL;
#+END_SRC
*** Fonctions 
**** Auxiliaires
#+BEGIN_SRC c
void Setup_MQTT_Task( void ){

   xTaskCreatePinnedToCore(
   MQTT_Task,
   "MQTT_Task",
   10000,
   NULL,
   1,
   &MQTTTaskHandle,
   1);

}
#+END_SRC
**** Principales
***** Initialisation (bloc Arduino =SETUP=)
#+BEGIN_SRC c
void setup() {
#+END_SRC

On commence par initialiser les ports série

#+BEGIN_SRC c
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial1.begin(9600);
#+END_SRC

Puis on active le système de fichier en mémoire Flash ( "Serial
Peripheral Interface Flash File System")

#+BEGIN_SRC c
  SPIFFS.begin();
  Serial.println("SPIFFS started");
#+END_SRC

Puis on active l'émulation d'EEPROM (?)
#+BEGIN_SRC c
  datastoresetup();
  #+END_SRC

Les broches d'entrée/sortie sont alors configurées  
#+BEGIN_SRC c
  pinMode(APPin, INPUT_PULLUP);
  pinMode(APLed, OUTPUT);
  pinMode(STALed, OUTPUT);
  pinMode(solarRelay, OUTPUT);
  pinMode(measBatt, ANALOG);
  #+END_SRC
  
et initialisées
#+BEGIN_SRC c
  digitalWrite(APLed, LOW);
  digitalWrite(STALed, LOW);
  digitalWrite(solarRelay, LOW);
 #+END_SRC

  
#+BEGIN_SRC c

  loadUploadSettings();
  loadNetworkCredentials();
  initWiFi();
  
  ws.initWindSensor();
  rs.initRainSensor();
  
  Wire.begin(25, 26, 100000); //sda, scl, freq=100kHz
  if(false == bme.begin(bmeAddress)){
        hasBME280 = false;
  } else {
    hasBME280 = true;
    //recommended settings for weather monitoring
    bme.setSampling(Adafruit_BME280::MODE_FORCED,
                    Adafruit_BME280::SAMPLING_X1, // temperature
                    Adafruit_BME280::SAMPLING_X1, // pressure
                    Adafruit_BME280::SAMPLING_X1, // humidity
                    Adafruit_BME280::FILTER_OFF);
  }

  #ifdef USE_SDS011 
    sds.setMode(SDS_SET_QUERY);
  #endif

  #ifdef USE_HONNYWHELLHPM
    hpm.begin();
  #endif
  Setup_MQTT_Task();
 
  
}
#+END_SRC 
***** Boucle principale (bloc Arduino =LOOP=)
#+BEGIN_SRC c
void loop() {
  //serial debugging
  checkSerial();
  
  NetworkTask();

  //if the current wifi mode isn't STA and the timout time has passed -> restart
  if ((WiFi.getMode() != WIFI_STA) && ((lastAPConnection + lastConnectedTimeout) < millis())) {
    if (digitalRead(APPin)) {
      Serial.println("Last connection was more than 10 minutes ago. Restart.");
      ESP.restart();
    }
    else {//button still pressed
      lastAPConnection = millis(); //wait some time before checking again
    }
  }

  //reinit if the wifi connection is lost
  if ((WiFi.getMode() == WIFI_STA) && (WiFi.status() == WL_DISCONNECTED)) {
    digitalWrite(STALed, LOW);
    Serial.println("Lost connection. Trying to reconnect.");
    initWiFi();
  }

  //read sensors
  readWindSensor();
  readRainSensor();

  //read bme280 every 5 seconds
  if ((lastBMETime + bmeInterval) < millis()) {
    lastBMETime = millis();
    readBME();
  }

  //read SDS011 every minute
  #ifdef USE_SDS011
  if ((lastSDSTime + sdsInterval) < millis()) {
    lastSDSTime = millis();
    if (!sds.getData(&PM25, &PM10))
      Serial.println("Failed to read SDS011");
  }
  #endif

  #ifdef HONNYWHELLHPM
    hpm.ProcessData();
    if ((lastSDSTime + sdsInterval) < millis()) {
      lastSDSTime = millis();
      if (!hpm.getData(&PM25, &PM10))
        Serial.println("Failed to read HPM");
      }
   
  #endif
  
  //upload data if the uploadperiod has passed and if WiFi is connected
  if (((lastUploadTime + uploadInterval) < millis()) && (WiFi.status() == WL_CONNECTED)) {
    lastUploadTime = millis();
    Serial.println("Upload interval time passed");
    
    windSpeedAvg = ws.getWindSpeedAvg();
    windDirAvg = ws.getWindDirAvg();
    rainAmountAvg = rs.getRainAmount() * hourMs / uploadInterval;
  
    if (thingspeakEnabled) {
      if (uploadToThingspeak())
        Serial.println("Uploaded successfully");
      else
        Serial.println("Uploading failed");
    }
    else
      Serial.println("Thingspeak disabled");

    if (senseBoxEnabled) {
      if (uploadToSenseBox())
        Serial.println("Uploaded successfully");
      else
        Serial.println("Uploading failed");
    }
    else
      Serial.println("SenseBox disabled");
  }

  //handle battery (resistor divider: vBatt|--[470k]-+-[100k]--|gnd)
  if ((lastBattMeasurement + battInterval) < millis()) {
    lastBattMeasurement = millis();
    float adcVoltage = ((float)analogRead(measBatt)/4096) * 3.3 + 0.15; //0.15 offset from real value
    batteryVoltage = adcVoltage * 570 / 100 + 0.7; //analog read between 0 and 3.3v * resistor divider + 0.7v diode drop
    //Serial.println("adc voltage: " + String(adcVoltage) + ", batt voltage: " + String(batteryVoltage) + ", currently charging: " + String(batteryCharging ? "yes" : "no"));
    if (batteryVoltage > battFull)
      batteryCharging = false;
    if (batteryVoltage < battLow)
      batteryCharging = true;
    
    digitalWrite(solarRelay, batteryCharging);
  }
}
#+END_SRC
**** Auxiliaires (suite)
#+BEGIN_SRC c
//reads the windsensor and stores the values in global variables
void readWindSensor() {
  if (digitalRead(windSpeedPin) && !prevWindPinVal) {
    ws.calcWindSpeed();
  }
  prevWindPinVal = digitalRead(windSpeedPin);

  ws.updateWindSensor();
  windSpeed = ws.getWindSpeed();
  beaufort = ws.getBeaufort();
  beaufortDesc = ws.getBeaufortDesc();

  ws.determineWindDir();
  windDir = ws.getWindDir();
}

void readRainSensor() {
  //inverted logic
  if (!digitalRead(rainPin) && prevRainPinVal) {
    Serial.println("Rainbucket tipped");
    rs.calcRainAmount();
  }
  prevRainPinVal = digitalRead(rainPin);
}

void readBME() {
  if( hasBME280 == true ){
    bme.takeForcedMeasurement();
    temperature = bme.readTemperature();
    humidity = bme.readHumidity();
    pressure = bme.readPressure() / 100.0;
  } else {
    humidity=0;
    pressure=0;
  }
}

void MQTT_Task( void* prarm ){
   const size_t capacity = 3*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(7);
   DynamicJsonDocument  root(capacity);
   String JsonString = "";
   uint32_t ulNotificationValue;
   int32_t last_message = millis();
   mqttsettings_t Settings = eepread_mqttsettings();
                         
   Serial.println("MQTT Thread Start");
   mqttclient.setCallback(callback);             // define Callback function
   while(1==1){

   /* if settings have changed we need to inform this task that a reload and reconnect is requiered */ 
   if(Settings.enable != false){
    ulNotificationValue = ulTaskNotifyTake( pdTRUE, 0 );
   } else {
    Serial.println("MQTT disabled, going to sleep");
    if(true == mqttclient.connected() ){
        mqttclient.disconnect();
    }
    ulNotificationValue = ulTaskNotifyTake( pdTRUE, portMAX_DELAY );
    Serial.println("MQTT awake from sleep");
   }

   if( ulNotificationValue&0x01 != 0 ){
      Serial.println("Reload MQTT Settings");
      /* we need to reload the settings and do a reconnect */
      if(true == mqttclient.connected() ){
        mqttclient.disconnect();
      }
      Settings = eepread_mqttsettings();
   }

   if(Settings.enable != false ) {
  
       if(!mqttclient.connected()) {             
            /* sainity check */
            if( (Settings.mqttserverport!=0) && (Settings.mqttservername[0]!=0) && ( Settings.enable != false ) ){
                  /* We try only every second to connect */
                  Serial.print("Connecting to MQTT...");  // connect to MQTT
                  mqttclient.setServer(Settings.mqttservername, Settings.mqttserverport); // Init MQTT     
                  if (mqttclient.connect(Settings.mqtthostname, Settings.mqttusername, Settings.mqttpassword)) {
                    Serial.println("connected");          // successfull connected  
                    mqttclient.subscribe(Settings.mqtttopic);             // subscibe MQTT Topic
                  } else {
                    Serial.println("failed");   // MQTT not connected       
                  }
            }
       } else{
            mqttclient.loop();                            // loop on client
            /* Check if we need to send data to the MQTT Topic, currently hardcode intervall */
            uint32_t intervall_end = last_message +( Settings.mqtttxintervall * 60000 );
            if( ( Settings.mqtttxintervall > 0) && ( intervall_end  <  millis() ) ){
              last_message=millis();
              JsonString="";
              /* Every minute we send a new set of data to the mqtt channel */
              JsonObject data = root.createNestedObject("data");            
              JsonObject data_wind = data.createNestedObject("wind");
              data_wind["direction"] = windDirAvg;
              data_wind["speed"] = windSpeedAvg;
              data["rain"] = rainAmountAvg;
              data["temperature"] = temperature;
              data["humidity"] = humidity;
              data["airpressure"] = pressure;
              data["PM2_5"] = PM25;
              data["PM10"] = PM10;
              
              JsonObject station = root.createNestedObject("station");
              station["battery"] = batteryVoltage;
              station["charging"] = batteryCharging;
              serializeJson(root,JsonString);
              Serial.println(JsonString);
              mqttclient.publish(Settings.mqtttopic, JsonString.c_str(), true); 
            }
       }
       vTaskDelay( 100/portTICK_PERIOD_MS );
   } 
 }
}

/***************************
 * callback - MQTT message
 ***************************/
void callback(char* topic, byte* payload, unsigned int length) {

}
#+END_SRC
** network.ino
#+BEGIN_SRC c
//network settings
#include "webfunctions.h"
#include <ESPmDNS.h>
#include <DNSServer.h>

#define DNS_PORT ( 53 )

DNSServer* dnsServer=NULL;

uint8_t macAddr[6];
char stringBufferAP[33]; 
String APSSID = "ESP32 XX:XX:XX";

String ssid;
String pass;

String SSIDList(String separator = ",") {
  Serial.println("Scanning networks");
  String ssidList;
  int n = WiFi.scanNetworks();
  for (int i = 0; i < n; i++) {
    String ssid = WiFi.SSID(i);
    Serial.println(String(i) + ": " + ssid);
    if (ssidList.indexOf(ssid) != -1) {
      Serial.println("SSID already in list");
    }
    else {
      if (ssidList != "")
        ssidList += separator;
      ssidList += ssid;
    }
  }
  return ssidList;
}

//send a list of available networks to the client connected to the webserver
void getSSIDList() {
  Serial.println("SSID list requested");
  sendData(SSIDList());
}

//store the wifi settings configured on the webpage and restart the esp to connect to this network
void setWiFiSettings() {
  Serial.println("WiFi settings received");
  Serial.println(server->uri());
  ssid = server->arg("ssid");
  pass = server->arg("pass");
  String response = "Attempting to connect to '" + ssid + "'. The WiFi module restarts and tries to connect to the network.";
  sendData(response);
  Serial.println("Saving network credentials and restart.");
  storeNetworkCredentials();
  delay(1000);
  ESP.restart();
}

//send the wifi settings to the connected client of the webserver
void getWiFiSettings() {
  Serial.println("WiFi settings requested");
  String response;
  response += ssid + ",";
  response += SSIDList(";");
  sendData(response);
}

//store the upload settings configured on the webpage
void setUploadSettings() {
  Serial.println("Upload settings received");
  Serial.println(server->uri());
  thingspeakApi = server->arg("tsApi");
  tsfWindSpeed = server->arg("tsfWS").toInt();
  tsfWindDir = server->arg("tsfWD").toInt();
  tsfRainAmount = server->arg("tsfRA").toInt();
  tsfTemperature = server->arg("tsfT").toInt();
  tsfHumidity = server->arg("tsfH").toInt();
  tsfAirpressure = server->arg("tsfA").toInt();
  tsfPM25 = server->arg("tsfPM25").toInt();
  tsfPM10 = server->arg("tsfPM10").toInt();
  thingspeakEnabled = (server->arg("tsEnabled") == "1");
  senseBoxStationId = server->arg("sbStationId");
  senseBoxWindSId = server->arg("sbWindSId");
  senseBoxWindDId = server->arg("sbWindDId");
  senseBoxRainId = server->arg("sbRainId");
  senseBoxTempId = server->arg("sbTempId");
  senseBoxHumId = server->arg("sbHumId");
  senseBoxPressId = server->arg("sbPressId");
  senseBoxPM25Id = server->arg("sbPM25Id");
  senseBoxPM10Id = server->arg("sbPM10Id");
  senseBoxEnabled = (server->arg("sbEnabled") == "1");
  long recvInterval = server->arg("interval").toInt();
  uploadInterval = ((recvInterval <= 0) ? hourMs : (recvInterval * 60 * 1000)); //convert to ms (default is 1 hr)
  storeUploadSettings();
  sendData("Upload settings stored");
}

void getUploadSettings() {
  Serial.println("Upload settings requested");
  String response;
  response += thingspeakApi + ",";
  response += String(tsfWindSpeed) + ",";
  response += String(tsfWindDir) + ",";
  response += String(tsfRainAmount) + ",";
  response += String(tsfTemperature) + ",";
  response += String(tsfHumidity) + ",";
  response += String(tsfAirpressure) + ",";
  response += String(tsfPM25) + ",";
  response += String(tsfPM10) + ",";
  response += String(thingspeakEnabled ? "1" : "0") + ",";
  response += senseBoxStationId + ",";
  response += senseBoxWindSId + ",";
  response += senseBoxWindDId + ",";
  response += senseBoxRainId + ",";
  response += senseBoxTempId + ",";
  response += senseBoxHumId + ",";
  response += senseBoxPressId + ",";
  response += senseBoxPM25Id + ",";
  response += senseBoxPM10Id + ",";
  response += String(senseBoxEnabled ? "1" : "0") + ",";
  response += String(uploadInterval / 1000 / 60); //convert to minutes
  sendData(response);
}

//send the weather data to the connected client of the webserver
void getWeatherData() {
  Serial.println("Weather data requested");
  String response;
  response += String(windSpeed*3.6) + ","; //km/h
  response += String(beaufort) + " (" + beaufortDesc + "),";
  response += String(windDir) + ",";
  response += String(temperature) + ",";
  response += String(humidity) + ",";
  response += String(pressure) + ",";
  response += String(PM25) + ",";
  response += String(PM10) +",";
  response += String(rainAmountAvg);
  sendData(response);
}

//send the battery data to the connected client of the webserver
void getBatteryData() {
  Serial.println("Battery data requested");
  String response;
  response += String(batteryVoltage) + ",";
  response += String(batteryCharging ? "1" : "0");
  sendData(response);
}

//toggle the charging of the battery
void toggleCharging() {
  Serial.println("Toggle charging requested");
  batteryCharging = !batteryCharging;
  sendData("Battery charging " + String(batteryCharging ? "started" : "stopped"));
}

//restart the esp as requested on the webpage
void restart() {
  sendData("The ESP32 will restart and you will be disconnected from the '" + APSSID + "' network.");
  delay(1000);
  ESP.restart();
}

//get the content type of a filename
String getContentType(String filename) {
  if (server->hasArg("download")) return "application/octet-stream";
  else if (filename.endsWith(".htm")) return "text/html";
  else if (filename.endsWith(".html")) return "text/html";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".js")) return "application/javascript";
  else if (filename.endsWith(".png")) return "image/png";
  else if (filename.endsWith(".gif")) return "image/gif";
  else if (filename.endsWith(".jpg")) return "image/jpeg";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  else if (filename.endsWith(".xml")) return "text/xml";
  else if (filename.endsWith(".pdf")) return "application/x-pdf";
  else if (filename.endsWith(".zip")) return "application/x-zip";
  else if (filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}

//send a file from the SPIFFS to the connected client of the webserver
void sendFile() {
  String path = server->uri();
  Serial.println("Got request for: " + path);
  if (path.endsWith("/")) path += "index.html";
  String contentType = getContentType(path);
  if (SPIFFS.exists(path)) {
    Serial.println("File " + path + " found");
    File file = SPIFFS.open(path, "r");
    server->streamFile(file, contentType);
    file.close();
  }
  else {
    Serial.println("File '" + path + "' doesn't exist");
    server->send(404, "text/plain", "The requested file doesn't exist");
  }
  
  lastAPConnection = millis();
}

//send data to the connected client of the webserver
void sendData(String data) {
  Serial.println("Sending: " + data);
  server->send(200, "text/plain", data);
  
  lastAPConnection = millis();
}

//initialize wifi by connecting to a wifi network or creating an accesspoint
void initWiFi() {
  digitalWrite(APLed, LOW);
  digitalWrite(STALed, LOW);
  Serial.print("WiFi: ");
  if (!digitalRead(APPin)) {
    Serial.println("AP");
    configureSoftAP();
  }
  else {
    Serial.println("STA");
    if (!connectWiFi()) {
      Serial.println("Connecting failed. Starting AP");
      configureSoftAP();
    }
    else {
      configureServer();
      configureClient();
      digitalWrite(STALed, HIGH);
    }
  }
}

//connect the esp32 to a wifi network
bool connectWiFi() {
  if (ssid == "") {
    Serial.println("SSID unknown");
    return false;
  }
  WiFi.mode(WIFI_STA);
  Serial.println("Attempting to connect to " + ssid + ", pass: " + pass);
  WiFi.begin(ssid.c_str(), pass.c_str());
  for (int timeout = 0; timeout < 15; timeout++) { //max 15 seconds
    int status = WiFi.status();
    if ((status == WL_CONNECTED) || (status == WL_CONNECT_FAILED) || (status == WL_NO_SSID_AVAIL))
      break;
    Serial.print(".");
    delay(1000);
  }
  Serial.println();
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Failed to connect to " + ssid);
    Serial.println("WiFi status: " + WiFiStatusToString());
    WiFi.disconnect();
    return false;
  }
  Serial.println("Connected to " + ssid);
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  
  return true;
}

//configure the access point of the esp32
void configureSoftAP() {
 
  WiFi.mode(WIFI_AP);
  WiFi.softAPmacAddress(macAddr);
  snprintf(stringBufferAP,32,"ESP32 Weatherstation %02x:%02x:%02x",macAddr[3],macAddr[4],macAddr[5]);
  APSSID = stringBufferAP;
  Serial.println("Configuring AP: " + String(APSSID));
  WiFi.softAP(APSSID.c_str(), NULL, 1, 0, 4);
  IPAddress ip = WiFi.softAPIP();
  Serial.print("AP IP: ");
  Serial.println(ip);
  lastAPConnection = millis();
  digitalWrite(APLed, HIGH);

  dnsServer = new DNSServer();
  dnsServer->start(DNS_PORT, "*", ip);

  /* Setup MDNS */
  if (!MDNS.begin("Weatherstation")) {
    Serial.println("Start MDNS: fail");   
  } else { 
    Serial.println("Start MDNS: okay");   
  }
  
  configureServer();

}

//initialize the webserver on port 80
void configureServer() {
  server = new WebServer(80);
  server->on("/getWeatherData", HTTP_GET, getWeatherData);
  server->on("/getBatteryData", HTTP_GET, getBatteryData);
  server->on("/toggleCharging", HTTP_GET, toggleCharging);
  server->on("/setWiFiSettings", HTTP_GET, setWiFiSettings);
  server->on("/getWiFiSettings", HTTP_GET, getWiFiSettings);
  server->on("/setUploadSettings", HTTP_GET, setUploadSettings);
  server->on("/getUploadSettings", HTTP_GET, getUploadSettings);
  server->on("/getSSIDList", HTTP_GET, getSSIDList);
  server->on("/restart", HTTP_GET, restart);
  register_functions(server);
  server->onNotFound(sendFile); //handle everything except the above things
  server->begin();
  Serial.println("Webserver started");
}

void configureClient() {
  client = new WiFiClient();
  clientS = new WiFiClientSecure();
  clientS->setCACert(certificate);
}

//request the specified url of the specified host
String performRequest(bool secure, String host, String url, int port = 80, String method = "GET", String headers = "Connection: close\r\n", String data = "") {
  WiFiClient* c = (secure ? clientS : client);
  Serial.println("Connecting to host '" + host + "' on port " + String(port));
  c->connect(host.c_str(), port); //default ports: http: port 80, https: 443
  String request = method + " " + url + " HTTP/1.1\r\n" +
                   "Host: " + host + "\r\n" +
                   headers + "\r\n";
  Serial.println("Requesting url: " + request);
  c->print(request);
  if (data != "") {
    Serial.println("Data: " + data);
    c->print(data + "\r\n");
  }
  
  unsigned long timeout = millis();
  while (c->available() == 0) {
    if (timeout + 5000 < millis()) {
      Serial.println("Client timeout");
      c->stop();
      return "";
    }
  }
  //read client reply
  String response;
  while(c->available()) {
    response = c->readStringUntil('\r');
  }
  Serial.println("Response: " + response);
  c->stop();
  return response;
}

String WiFiStatusToString() {
  switch (WiFi.status()) {
    case WL_IDLE_STATUS:     return "IDLE"; break;
    case WL_NO_SSID_AVAIL:   return "NO SSID AVAIL"; break;
    case WL_SCAN_COMPLETED:  return "SCAN COMPLETED"; break;
    case WL_CONNECTED:       return "CONNECTED"; break;
    case WL_CONNECT_FAILED:  return "CONNECT_FAILED"; break;
    case WL_CONNECTION_LOST: return "CONNECTION LOST"; break;
    case WL_DISCONNECTED:    return "DISCONNECTED"; break;
    case WL_NO_SHIELD:       return "NO SHIELD"; break;
    default:                 return "Undefined: " + String(WiFi.status()); break;
  }
}

void NetworkTask( void ){
  //handle WiFi
  if (server != NULL){
    server->handleClient();
  }
  
  if(dnsServer!=NULL){
     dnsServer->processNextRequest();
  }
  
}
#+END_SRC
** serialHandler.ino

#+BEGIN_SRC c
void checkSerial() {
  while (Serial.available()) {
    char in = Serial.read();
    serialIn += in;
    if (in == '\n')
      serialRdy = true;
  }
  handleSerial();
}

void handleSerial() {
  if (serialRdy) {
    Serial.println(serialIn);
    if (serialIn.indexOf("thingspeakUpload") != -1) {
      if (uploadToThingspeak())
        Serial.println("Success");
      else
        Serial.println("Failed");
    }
    else if (serialIn.indexOf("senseBoxUpload") != -1) {
      if (uploadToSenseBox())
        Serial.println("Success");
      else
        Serial.println("Failed");
    }
    else if (serialIn.indexOf("wifiStatus") != -1) {
      Serial.println("WiFi status: " + WiFiStatusToString());
    }
    else if (serialIn.indexOf("printData") != -1) {
      Serial.println("Wind speed:         " + String(ws.getWindSpeed()) + "m/s, " + String(ws.getWindSpeed() * 3.6) + "km/h");
      Serial.println("Beaufort:           " + String(ws.getBeaufort()) + " (" + ws.getBeaufortDesc() + ")");
      Serial.println("Wind speed avg:     " + String(ws.getWindSpeedAvg(false)));
      Serial.println("Wind direction:     " + ws.getWindDirString() + " (" + String(ws.getWindDir()) + ")");
      Serial.println("Wind direction avg: " + String(ws.getWindDirAvg(false)));
      Serial.println("Rain amount:        " + String(rs.getRainAmount(false)) + "mm");
      Serial.println("Temperature:        " + String(temperature) + "*C");
      Serial.println("Humidity:           " + String(humidity) + "%");
      Serial.println("Pressure:           " + String(pressure) + "hPa");
      Serial.println("Dust 10um:          " + String(PM10) + "ug/m3");
      Serial.println("Dust 2.5um:         " + String(PM25) + "ug/m3");
    }
    else if (serialIn.indexOf("WifiSettings") != -1) {
      if (serialIn.indexOf("set") != -1) {
        ssid = serialIn.substring(serialIn.indexOf(":") + 1, serialIn.indexOf(","));
        pass = serialIn.substring(serialIn.indexOf(",") + 1, serialIn.indexOf("\n"));
        storeNetworkCredentials();
      }
      Serial.println("WiFi settings: SSID: " + ssid + ", pass: " + pass);
    }
    else if (serialIn.indexOf("thingspeakSettings") != -1) {
      Serial.println("Thingspeak settings:");
      Serial.println("  Api key:  " + thingspeakApi);
      Serial.println("  Field numbers:");
      Serial.println("    Wind speed:     " + String(tsfWindSpeed));
      Serial.println("    Wind direction: " + String(tsfWindDir));
      Serial.println("    Rain amount:    " + String(tsfRainAmount));
      Serial.println("    Temperature:    " + String(tsfTemperature));
      Serial.println("    Humidity:       " + String(tsfHumidity));
      Serial.println("    Airpressure:    " + String(tsfAirpressure));
      Serial.println("    PM25:           " + String(tsfPM25));
      Serial.println("    PM10:           " + String(tsfPM10));
      Serial.println("  Enabled:  " + String(thingspeakEnabled ? "Yes" : "No"));
      Serial.println("  Interval: " + String(uploadInterval) + "ms (" + String(uploadInterval/1000/60) + " minutes)");
    }
    else if (serialIn.indexOf("senseBoxSettings") != -1) {
      Serial.println("SenseBox settings:");
      Serial.println("  Station ID:     " + senseBoxStationId);
      Serial.println("  Wind speed ID:  " + senseBoxWindSId);
      Serial.println("  Wind dir ID:    " + senseBoxWindDId);
      Serial.println("  Rain ID:        " + senseBoxRainId);
      Serial.println("  temperature ID: " + senseBoxTempId);
      Serial.println("  Humidity ID:    " + senseBoxHumId);
      Serial.println("  Pressure ID:    " + senseBoxPressId);
      Serial.println("  Enabled:        " + String(senseBoxEnabled ? "Yes" : "No"));
      Serial.println("  Interval: " + String(uploadInterval) + "ms (" + String(uploadInterval/1000/60) + " minutes)");
    }
    else if (serialIn.indexOf("restart") != -1) {
      ESP.restart();
    }
    else if (serialIn.indexOf("timeActive") != -1) {
      unsigned long time = millis();
      Serial.println("Time active: " + String(time) + "ms, " + String(time/1000/60) + " minutes");
    }
    
    serialRdy = false;
    serialIn = "";
  }
}
#+END_SRC

** storePerfs.ino

#+BEGIN_SRC c
/*
 * Note: preference names can't be longer than 15 characters
 */
void loadNetworkCredentials() {
  pref.begin("weather", false);
  ssid = pref.getString("ssid");
  pass = pref.getString("pass");
  pref.end();
}

void storeNetworkCredentials() {
  pref.begin("weather", false);
  pref.putString("ssid", ssid);
  pref.putString("pass", pass);
  pref.end();
}

void loadUploadSettings() {
  pref.begin("weather", false);
  thingspeakEnabled = pref.getBool("thingspeakEn");
  thingspeakApi = pref.getString("thingspeakApi");
  tsfWindSpeed = pref.getInt("tsfWindSpeed");
  tsfWindDir = pref.getInt("tsfWindDir");
  tsfRainAmount = pref.getInt("tsfRainAmount");
  tsfTemperature = pref.getInt("tsfTemperature");
  tsfHumidity = pref.getInt("tsfHumidity");
  tsfAirpressure = pref.getInt("tsfAirpressure");
  tsfPM25 = pref.getInt("tsfPM25");
  tsfPM10 = pref.getInt("tsfPM10");

  senseBoxEnabled = pref.getBool("sbEnabled");
  senseBoxStationId = pref.getString("sbStationId");
  senseBoxTempId = pref.getString("sbTempId");
  senseBoxHumId = pref.getString("sbHumId");
  senseBoxPressId = pref.getString("sbPressId");
  senseBoxWindSId = pref.getString("sbWindSId");
  senseBoxWindDId = pref.getString("sbWindDId");
  senseBoxRainId = pref.getString("sbRainId");
  senseBoxPM25Id = pref.getString("sbPM25Id");
  senseBoxPM10Id = pref.getString("sbPM10Id");
  
  uploadInterval = ((pref.getUInt("uploadInterval") != 0) ? pref.getUInt("uploadInterval") : hourMs);
  pref.end();
}

void storeUploadSettings() {
  pref.begin("weather", false);
  pref.putBool("thingspeakEn", thingspeakEnabled);
  pref.putString("thingspeakApi", thingspeakApi);
  pref.putInt("tsfWindSpeed", tsfWindSpeed);
  pref.putInt("tsfWindDir", tsfWindDir);
  pref.putInt("tsfRainAmount", tsfRainAmount);
  pref.putInt("tsfTemperature", tsfTemperature);
  pref.putInt("tsfHumidity", tsfHumidity);
  pref.putInt("tsfAirpressure", tsfAirpressure);
  pref.putInt("tsfPM25", tsfPM25);
  pref.putInt("tsfPM10", tsfPM10);

  pref.putBool("sbEnabled", senseBoxEnabled);
  pref.putString("sbStationId", senseBoxStationId);
  pref.putString("sbTempId", senseBoxTempId);
  pref.putString("sbHumId", senseBoxHumId);
  pref.putString("sbPressId", senseBoxPressId);
  pref.putString("sbWindSId", senseBoxWindSId);
  pref.putString("sbWindDId", senseBoxWindDId);
  pref.putString("sbRainId", senseBoxRainId);
  pref.putString("sbPM25Id", senseBoxPM25Id);
  pref.putString("sbPM10Id", senseBoxPM10Id);
  
  pref.putUInt("uploadInterval", uploadInterval);
  pref.end();
}
#+END_SRC
** uploadData.ino
#+BEGIN_SRC
//upload the sensor data to thingspeak
bool uploadToThingspeak() {
  Serial.println("Uploading to thingspeak");
  if (thingspeakApi == "") {
    Serial.println("Thingspeak API key not set");
    return false;
  }
  
  printUploadValues();
  
  String thingspeakHost = "api.thingspeak.com";
  String thingspeakUrl = "/update";
  thingspeakUrl += "?api_key=" + thingspeakApi;
  if (tsfWindSpeed != 0)
    thingspeakUrl += "&field" + String(tsfWindSpeed) + "=" + String(windSpeedAvg*3.6); //km/h
  if (tsfWindDir != 0)
    thingspeakUrl += "&field" + String(tsfWindDir) + "=" + String(windDirAvg);
  if (tsfRainAmount != 0)
    thingspeakUrl += "&field" + String(tsfRainAmount) + "=" + String(rainAmountAvg);
  if (tsfTemperature != 0)
    thingspeakUrl += "&field" + String(tsfTemperature) + "=" + String(temperature);
  if (tsfHumidity != 0)
    thingspeakUrl += "&field" + String(tsfHumidity) + "=" + String(humidity);
  if (tsfAirpressure != 0)
    thingspeakUrl += "&field" + String(tsfAirpressure) + "=" + String(pressure);
  if (tsfPM25 != 0)
    thingspeakUrl += "&field" + String(tsfPM25) + "=" + String(PM25);
  if (tsfPM10 != 0)
    thingspeakUrl += "&field" + String(tsfPM10) + "=" + String(PM10);

  String resp = performRequest(false, thingspeakHost, thingspeakUrl);
  return (resp != "" && !resp.startsWith("0"));
}

//upload the sensor values to senseBox
bool uploadToSenseBox() {
  Serial.println("Uploading to SenseBox");
  if (senseBoxStationId == "") {
    Serial.println("SenseBox station ID not set");
    return false;
  }
  
  printUploadValues();
  
  String csv;
  if (senseBoxWindSId != "")
    csv += senseBoxWindSId + "," + String(windSpeedAvg*3.6) + "\r\n"; //km/h
  if (senseBoxWindDId != "")
    csv += senseBoxWindDId + "," + String(windDirAvg) + "\r\n";
  if (senseBoxRainId != "")
    csv += senseBoxRainId + "," + String(rainAmountAvg) + "\r\n";
  if (senseBoxTempId != "")
    csv += senseBoxTempId + "," + String(temperature) + "\r\n";
  if (senseBoxHumId != "")
    csv += senseBoxHumId + "," + String(humidity) + "\r\n";
  if (senseBoxPressId != "")
    csv += senseBoxPressId + "," + String(pressure) + "\r\n";
  if (senseBoxPM25Id != "")
    csv += senseBoxPM25Id + "," + String(PM25) + "\r\n";
  if (senseBoxPM10Id != "")
    csv += senseBoxPM10Id + "," + String(PM10) + "\r\n";

  if (csv == "") {
    Serial.println("Sensor API keys not set");
    return false;
  }
  
  String senseBoxHost = "api.opensensemap.org";
  String senseBoxUrl = "/boxes/" + senseBoxStationId + "/data";
  String headers = "Content-Type: text/csv\r\n";
  headers += "Connection: close\r\n";
  headers += "Content-Length: " + String(csv.length()) + "\r\n";

  String resp = performRequest(true, senseBoxHost, senseBoxUrl, 443, "POST", headers, csv);
  return true;
}

void printUploadValues() {
  Serial.println("Values:");
  Serial.println("WindSpeedAvg:  " + String(windSpeedAvg));
  Serial.println("WindDirAvg:    " + String(windDirAvg));
  Serial.println("RainAmountAvg: " + String(rainAmountAvg));
  Serial.println("Temperature:   " + String(temperature));
  Serial.println("Humidity:      " + String(humidity));
  Serial.println("Pressure:      " + String(pressure));
  Serial.println("PM25:          " + String(PM25));
  Serial.println("PM10:          " + String(PM10));
}
#+END_SRC

* Les bibliothèques
** En-têtes
  #+BEGIN_SRC sh :results verbatim :exports yes
  tree esp32weatherstation -I build -P *.h -L 1 --prune
  #+END_SRC 

  #+RESULTS:
  : esp32weatherstation
  : ├── datastore.h
  : ├── Honnywhell.h
  : ├── RainSensor.h
  : ├── sslCertificate.h
  : ├── webfunctions.h
  : └── WindSensor.h
  : 
  : 0 directories, 6 files
** Code
  #+BEGIN_SRC sh :results verbatim :exports yes
  tree esp32weatherstation -I build -P *.cpp -L 1 --prune
  #+END_SRC 

  #+RESULTS:
  : esp32weatherstation
  : ├── datastore.cpp
  : ├── Honnywhell.cpp
  : ├── RainSensor.cpp
  : ├── webfunctions.cpp
  : └── WindSensor.cpp
  : 
  : 0 directories, 5 files
