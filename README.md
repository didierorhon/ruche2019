Prochaine rencontre :
=====================
    - 26/09/2019 :: Bilan du test de calibration balance

Date limite :
    - Fin projet :: Juin 2019
    
À faire :
=====================
    - Balance :
        - Module :
            -Apprendre à calibrer
            - Campagne de mesures pour déterminer une fonction de calcul du poids (sur Arduino)
            - Bascule de la balance sur module TTGO et tests
            - Tests sur TTGO avec mode sommeil profond
        - Achats: 
            - ...
        - Bricolage :
            - réalisation d'un plateau adapté au socle de la ruche

    - Capteurs température et humidité
        - Module :
            - Test DHT22 sur TTGO
        - Bricolage :
            - réalisation de l'intégration au coeur
            - réalisation de l'intégration au externe
            
    - LoRa
        - Code : 
            - programmation du transfert LoRa
        - Installation :
            - Test autre lieux
            - Test cimenterie
            
    - Optimisation énergétique
        - Mesure consommation TTGO sans LoRa
            - sans mode sommeil
            - avec mode sommeil
        - Mesure consommation TTGO avec LoRa 
            - sans mode sommeil
            - avec mode sommeil
        - Détermination capacité batterie
    
    - Panneau solaire
        - Voir nécessité
        - Choix éventuel
        - Tests
        
    -     
Suivi des rencontres :
=====================
    - 19/09/2019 :: première rencontre et tests balance
    