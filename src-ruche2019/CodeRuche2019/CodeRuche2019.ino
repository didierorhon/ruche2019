/*****************************************************
 *
 * Ruche 2019
 *   
 *   Utilise les bibliothèques
 *   - ArduinoDebugUtil
 *   - DHTesp
 *   - BME280
 ******************************************************/

/* Inclusions */
// Déboggage
#include <Arduino_DebugUtils.h>

// Capteur DHT22
// https://github.com/beegee-tokyo/DHTesp
#include "./bibliotq/DHTesp/DHTesp.h"

// Capteur BME280
// https://github.com/finitespace/BME280
#include "./bibliotq/BME280/src/BME280I2C.h"

// Gestionnaire d'évènements périodiques 
// Bibliothèque ESP32-Arduino
#include "Ticker.h"

/* Macros */
#define BAUDRATE 115200

/* Variables globales */
DHTesp dhtSensor1 ;
DHTesp dhtSensor2 ;
int dhtPin1 = 17;
int dhtPin2 = 16;

TaskHandle_t tempTaskHandle = NULL;
Ticker tempTicker;
bool gotNewTemperature = false;
TempAndHumidity sensor1Data;
/** Data from sensor 2 */
TempAndHumidity sensor2Data;
bool tasksEnabled = false;

/* Paramètres de compilation */

/* Fonctions */
/* Auxiliaires */
void tempTask(void *pvParameters) {
  Serial.println("tempTask loop started");
  while (1) // tempTask loop
  {
    if (tasksEnabled && !gotNewTemperature) { // Read temperature only if old data was processed already
      // Reading temperature for humidity takes about 250 milliseconds!
      // Sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
      sensor1Data = dhtSensor1.getTempAndHumidity();  // Read values from sensor 1
      sensor2Data = dhtSensor2.getTempAndHumidity();  // Read values from sensor 1
      gotNewTemperature = true;
    }
    vTaskSuspend(NULL);
  }
}

void triggerGetTemp() {
  if (tempTaskHandle != NULL) {
    xTaskResumeFromISR(tempTaskHandle);
  }
}

void initDHT() {
  dhtSensor1.setup(dhtPin1, DHTesp::DHT22);
  dhtSensor2.setup(dhtPin2, DHTesp::DHT22);
  Debug.print(DBG_INFO, "Capteurs DHT initialisés");
}

void initDebug() {
  Serial.begin(BAUDRATE);
  Debug.timestampOn();
  Debug.print(DBG_INFO, "Port série actif");
}

void initTempTask() {
  xTaskCreatePinnedToCore(
    tempTask,                      /* Function to implement the task */
    "tempTask ",                    /* Name of the task */
    4000,                          /* Stack size in words */
    NULL,                          /* Task input parameter */
    5,                              /* Priority of the task */
    &tempTaskHandle,                /* Task handle. */
    1);                            /* Core where the task should run */
  if (tempTaskHandle == NULL) {
    Serial.println("[ERROR] Failed to start task for temperature update");
  } else {
    // Start update of environment data every 30 seconds
    tempTicker.attach(30, triggerGetTemp);
  }
  tasksEnabled = true;

}
