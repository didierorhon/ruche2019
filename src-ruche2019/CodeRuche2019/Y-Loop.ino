void loop() {
  if (gotNewTemperature) {
    Serial.println("Sensor 1 data:");
    Serial.println("Temp: " + String(sensor1Data.temperature, 2) + "'C Humidity: " + String(sensor1Data.humidity, 1) + "%");
    Serial.println("Sensor 2 data:");
    Serial.println("Temp: " + String(sensor2Data.temperature, 2) + "'C Humidity: " + String(sensor2Data.humidity, 1) + "%");
    gotNewTemperature = false;
  }

}
