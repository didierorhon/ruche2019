#include <HX711.h>

HX711 scale(A1, A0) ;

unsigned int numberOfData = 10 ;

void batch(float masse_test) {
  Serial.println();
  Serial.print(masse_test) ;
  Serial.print(",") ;
  for (int i = 0 ; i < numberOfData ; i++) {
    Serial.print(scale.read_average(numberOfData)) ;
    Serial.print(",") ;
  }
  Serial.println() ;
}

void setup() {
  Serial.begin(115200) ;
  Serial.println() ;
  Serial.println("Allume balance") ;
  scale.power_up() ;
  Serial.println("Mise à zéro des paramètres") ;
  scale.set_offset(0) ;
  scale.set_scale(100.f) ;
  scale.power_down();
  Serial.println("Enveler le plateau") ;
  delay(5000);
  batch(0);
  
  Serial.println("Remettre le plateau") ;
  delay(5000);
  batch(-1); // Indiquer masse plateau
}

void loop() {
  Serial.println("Placer maintenant une masse connue sur le plateau...");
  Serial.print("Entrer la valeur de la masse (en kg) puis presser valider:");
  unsigned int weight;
  while (!Serial.available()) {} ;
  weight = Serial.parseInt();
  delay(5000);
  batch(weight);
  Serial.flush();
}
