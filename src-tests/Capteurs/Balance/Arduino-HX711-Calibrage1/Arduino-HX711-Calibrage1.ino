/*
   Programme de calibration de la balance
   Utilise :
    - Arduino UNO
    - HX711
    - Balance en version de test
*/
/*
     Procédure de calibration : (voir fichier README.md livré avec la bibliothèque
  # How to Calibrate Your Scale
  1. Call set_scale() with no parameter.
  2. Call tare() with no parameter.
  3. Place a known weight on the scale and call get_units(10).
  4. Divide the result in step 3 to your known weight. You should get about the parameter you need to pass to set_scale.
  5. Adjust the parameter in step 4 until you get an accurate reading.

*/
#include "HX711.h"
/*
    REMARQUE:
    - HX711.DOUT  - pin #A1 [SDA]
    - HX711.PD_SCK - pin #A0 [SCL]

*/
HX711 scale(A1, A0 , 128); // DOUT, SDK, GAIN

void read_and_display_values() {
  Serial.print(scale.read());
  Serial.print("\t");
  Serial.print(scale.read_average(20));
  Serial.print("\t");
  Serial.print(scale.get_value(20));
  Serial.print("\t");
  Serial.println(scale.get_units(20));
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println("Calibration HX711");
  Serial.println();
  Serial.println();
  scale.power_up();

  Serial.println("Résultats avant calibration:");
  read_and_display_values();

  Serial.println("1er étape de calibration");
  scale.set_scale();
  read_and_display_values();

  Serial.println("2nd étape de calibration");
  scale.tare();
  read_and_display_values();

  Serial.println("Placer maintenant une masse connue sur le plateau...");
  Serial.print("Entrer la valeur de la masse (en kg) puis presser valider:");
  unsigned int weight;
  while (!Serial.available()) {} ;
  weight = Serial.parseInt();
  Serial.println("");
  Serial.print("Essai avec une masse de :");
  Serial.print(weight);
  Serial.println("kg.");

  Serial.println("Attente stabilisation -- 5 secondes");
  delay(5000);
  unsigned int valRaw = scale.get_units(10);
  Serial.print("Valeur obtenue sur 10 mesures:");
  Serial.println(valRaw);

  float calFactor = float(valRaw) / float(weight);
  Serial.print("Facteur de calibration obtenu:");
  Serial.println(calFactor, 2);

  Serial.println();
  Serial.println("Application du facteur de calibration:");
  scale.set_scale(calFactor);

  Serial.print("Relecture avec une masse de :");
  Serial.print(weight);
  Serial.println("kg.");
  Serial.println("Attente stabilisation -- 5 secondes");
  delay(5000);
  Serial.print("Valeur obtenue sur 10 mesures:");
  Serial.println(scale.get_units(10));

  Serial.println();
  Serial.println("Extinction HX711 pour 5 secondes");
  scale.power_down();
  delay(5000);
  scale.power_up();
  
  Serial.println();
  Serial.println("Allumage et nouveau test.");
  Serial.print("Relecture avec une masse de :");
  Serial.print(weight);
  Serial.println("kg.");
  Serial.println("Attente stabilisation -- 5 secondes");
  delay(5000);
  Serial.print("Valeur obtenue sur 10 mesures:");
  Serial.println(scale.get_units(10));



}

void loop() {
}
